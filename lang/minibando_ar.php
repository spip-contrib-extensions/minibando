<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minibando?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// M
	'mode_debug' => 'وضعية كشف الأخطاء',
	'mode_inclure' => 'وضعية الادراج',
	'mode_profile' => 'وضعية المعلومات الشخصية',
	'mode_traduction' => 'وضعية الترجمة',

	// T
	'titre_debug' => 'كشف الأخطاء',
	'titre_outils_rapides' => 'إنشاء',
];
