<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minibando?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_titre_minibando' => 'Settings',

	// L
	'label_disposition' => 'Layout',
	'label_disposition_horizontale' => 'Horizontal',
	'label_disposition_verticale' => 'Vertical',
	'label_limite' => 'Restriction',
	'label_limite_webmestre' => 'Display minibando for webmasters only',

	// M
	'mode_css' => 'Css mode',
	'mode_debug' => 'Debug mode',
	'mode_inclure' => 'Inclusion mode',
	'mode_profile' => 'Profiling mode',
	'mode_traduction' => 'Translation mode',

	// T
	'titre_debug' => 'Debug',
	'titre_outils_rapides' => 'Creation',
	'titre_page_configurer_minibando' => 'Configure minibando plugin',
];
