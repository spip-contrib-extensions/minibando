<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minibando?lang_cible=eu
// ** ne pas modifier le fichier **

return [

	// M
	'mode_debug' => 'Debug modua',
	'mode_inclure' => '<i>include</i> modua',
	'mode_profile' => '<i>profile</i> modua',

	// T
	'titre_debug' => 'Debug',
	'titre_outils_rapides' => 'Sorkuntza', # MODIF
];
