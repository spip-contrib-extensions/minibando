<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minibando?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// M
	'mode_debug' => 'حالت خطاياب',
	'mode_inclure' => 'حالت گنجاندن',
	'mode_profile' => 'حالت پروفايل',

	// T
	'titre_debug' => 'خطازدايي',
	'titre_outils_rapides' => 'آفرينش', # MODIF
];
