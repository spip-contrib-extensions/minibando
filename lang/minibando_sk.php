<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minibando?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// M
	'mode_debug' => 'Režim ladenia',
	'mode_inclure' => 'Režim vkladania',
	'mode_profile' => 'Režim profilovania',
	'mode_traduction' => 'Režim prekladu',

	// T
	'titre_debug' => 'Ladiť',
	'titre_outils_rapides' => 'Vytvorenie',
];
