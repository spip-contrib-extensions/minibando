<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-minibando?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// M
	'minibando_description' => 'يستبدل هذا الملحق أزرار الإدارة في SPIP بشريط أدوات يحتوي على العناصر المتوافرة في التصفح الأعلى في المجال الخاص. ',
	'minibando_slogan' => 'شريط أدوات صغير للكثير من الوظائف!',
];
