<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-minibando?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// M
	'minibando_description' => 'The minibando plugin replaces SPIP’s administration buttons with a toolbar that mirrors that of the private area.',
	'minibando_slogan' => 'A tiny menu for maximum functionality',
];
