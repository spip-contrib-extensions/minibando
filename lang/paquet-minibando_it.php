<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-minibando?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// M
	'minibando_description' => 'Il plugin minibando sostituisce i pulsanti di amministrazione di SPIP con una barra degli strumenti che contiene gli elementi disponibili nella navigazione in alto dell’area riservata di SPIP.',
	'minibando_slogan' => 'Una piccola barra ed il massimo delle funzionalità!',
];
